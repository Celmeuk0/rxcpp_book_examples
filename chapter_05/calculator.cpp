#include <functional>
#include <future>
#include <iostream>
#include <list>
#include <memory>
#include <random>
#include <stack>
#include <thread>
using namespace std;

//---------------------List of operators supported by the evaluator
enum class OPERATOR
{
    ILLEGAL,
    PLUS,
    MINUS,
    MUL,
    DIV,
    UNARY_PLUS,
    UNARY_MINUS
};

//------------ forward declarations for the Composites
class Number;        //----- Stores IEEE double precision floating point number
class BinaryExpr;    //--- Node for Binary Expression
class UnaryExpr;     //--- Node for Unary Expression
struct IExprVisitor; //---- Interface for the Visitor

//---- Every node in the expression tree will inherit from the Expr class
class Expr
{
  public:
    //---- The standard Visitor double dispatch method
    //---- Normally return value of accept method are void.... and Concrete
    //---- classes store the result which can be retrieved later
    virtual double accept(IExprVisitor& expr_vis) = 0;
    virtual ~Expr();
};

//----- The Visitor interface contains methods for each of the concrete node
//----- Normal practice is to use
struct IExprVisitor
{
    virtual ~IExprVisitor();
    virtual double Visit(Number& num) = 0;
    virtual double Visit(BinaryExpr& bin) = 0;
    virtual double Visit(UnaryExpr& un) = 0;
};

//---------A class to represent IEEE 754 interface
class Number : public Expr
{
    double NUM;

  public:
    double getNUM() { return NUM; }
    void setNUM(double num) { NUM = num; }
    Number(double n) { this->NUM = n; }
    ~Number() override;

    double accept(IExprVisitor& expr_vis) override { return expr_vis.Visit(*this); }
};

//-------------- Modeling Binary Expresison
class BinaryExpr : public Expr
{
    Expr* left;
    Expr* right;
    OPERATOR OP;

  public:
    BinaryExpr(Expr* l, Expr* r, OPERATOR op)
    {
        left = l;
        right = r;
        OP = op;
    }
    OPERATOR getOP() { return OP; }
    Expr& getLeft() { return *left; }
    Expr& getRight() { return *right; }
    ~BinaryExpr() override;
    double accept(IExprVisitor& expr_vis) override { return expr_vis.Visit(*this); }
};

//-----------------Modeling Unary Expression
class UnaryExpr : public Expr
{
    Expr* right;
    OPERATOR op;

  public:
    UnaryExpr(Expr* operand, OPERATOR op)
    {
        right = operand;
        this->op = op;
    }
    Expr& getRight() { return *right; }
    OPERATOR getOP() { return op; }
    virtual ~UnaryExpr() override;
    double accept(IExprVisitor& expr_vis) override { return expr_vis.Visit(*this); }
};

//--------An Evaluator for Expression Composite using Visitor Pattern
class TreeEvaluatorVisitor : public IExprVisitor
{
  public:
    ~TreeEvaluatorVisitor() override;

    double Visit(Number& num) override { return num.getNUM(); }
    double Visit(BinaryExpr& bin) override
    {
        OPERATOR temp = bin.getOP();
        double lval = bin.getLeft().accept(*this);
        double rval = bin.getRight().accept(*this);
        return (temp == OPERATOR::PLUS)
                 ? lval + rval
                 : (temp == OPERATOR::MUL)
                     ? lval * rval
                     : (temp == OPERATOR::DIV) ? lval / rval : lval - rval;
    }
    double Visit(UnaryExpr& un) override
    {
        OPERATOR temp = un.getOP();
        double rval = un.getRight().accept(*this);
        return (temp == OPERATOR::UNARY_PLUS) ? +rval : -rval;
    }
};

//------------A Visitor to Print Expression in RPN
class ReversePolishEvaluator : public IExprVisitor
{
  public:
    ~ReversePolishEvaluator() override;

    double Visit(Number& num) override
    {
        cout << num.getNUM() << " " << endl;
        return 42;
    }
    double Visit(BinaryExpr& bin) override
    {
        bin.getLeft().accept(*this);
        bin.getRight().accept(*this);
        OPERATOR temp = bin.getOP();
        cout << ((temp == OPERATOR::PLUS)
                   ? " + "
                   : (temp == OPERATOR::MUL) ? " * "
                                             : (temp == OPERATOR::DIV) ? " / " : " - ");
        return 42;
    }
    double Visit(UnaryExpr& un) override
    {
        OPERATOR temp = un.getOP();
        un.getRight().accept(*this);
        cout << ((temp == OPERATOR::UNARY_PLUS) ? " (+) " : " (-) ");
        return 42;
    }
};

Expr::~Expr()
{}

IExprVisitor::~IExprVisitor()
{}

ReversePolishEvaluator::~ReversePolishEvaluator()
{}

TreeEvaluatorVisitor::~TreeEvaluatorVisitor()
{}

Number::~Number()
{}

BinaryExpr::~BinaryExpr()
{
    delete left;
    delete right;
    left = nullptr;
    right = nullptr;
}

UnaryExpr::~UnaryExpr()
{
    delete right;
    right = nullptr;
}

////////////////////////////
// A enum to store discriminator -> Operator or a Value?
enum class ExprKind
{
    ILLEGAL_EXP,
    OPERATOR,
    VALUE
};

// A Data structure to store the Expression node.
// A node will either be a Operator or Value
struct EXPR_ITEM
{
    ExprKind knd;
    double Value;
    OPERATOR op;

    EXPR_ITEM() : knd(ExprKind::ILLEGAL_EXP), Value(0), op(OPERATOR::ILLEGAL) {}
    bool SetOperator(OPERATOR op)
    {
        this->op = op;
        this->knd = ExprKind::OPERATOR;
        return true;
    }
    bool SetValue(double value)
    {
        this->knd = ExprKind::VALUE;
        this->Value = value;
        return true;
    }
    string toString()
    {
        DumpContents();
        return "";
    }

  private:
    void DumpContents() {} //---- Code omitted for brevity
};

//---- A Flattener for Expressions
class FlattenVisitor : public IExprVisitor
{
    list<EXPR_ITEM> ils;
    EXPR_ITEM MakeListItem(double num)
    {
        EXPR_ITEM temp;
        temp.SetValue(num);
        return temp;
    }
    EXPR_ITEM MakeListItem(OPERATOR op)
    {
        EXPR_ITEM temp;
        temp.SetOperator(op);
        return temp;
    }

  public:
    list<EXPR_ITEM> FlattenedExpr() { return ils; }
    FlattenVisitor() {}
    ~FlattenVisitor() override;

    double Visit(Number& num) override
    {
        ils.push_back(MakeListItem(num.getNUM()));
        return 42;
    }

    double Visit(BinaryExpr& bin) override
    {
        bin.getLeft().accept(*this);
        bin.getRight().accept(*this);
        ils.push_back(MakeListItem(bin.getOP()));
        return 42;
    }

    double Visit(UnaryExpr& un) override
    {
        un.getRight().accept(*this);
        ils.push_back(MakeListItem(un.getOP()));
        return 42;
    }
};

list<EXPR_ITEM> ExprList(Expr* r)
{
    unique_ptr<FlattenVisitor> fl(new FlattenVisitor());
    r->accept(*fl);
    list<EXPR_ITEM> ret = fl->FlattenedExpr();
    return ret;
}

//-------- A minimal stack to evaluate RPN expression
class DoubleStack : public stack<double>
{
  public:
    DoubleStack() {}
    void Push(double a) { this->push(a); }
    double Pop()
    {
        double a = this->top();
        this->pop();
        return a;
    }
};

//------Iterator through eachn element of Expression list
double Evaluate(list<EXPR_ITEM> ls)
{
    DoubleStack stk;
    double n;
    for (EXPR_ITEM s : ls) {
        if (s.knd == ExprKind::VALUE) {
            stk.Push(s.Value);
        } else if (s.op == OPERATOR::PLUS) {
            stk.Push(stk.Pop() + stk.Pop());
        } else if (s.op == OPERATOR::MINUS) {
            stk.Push(stk.Pop() - stk.Pop());
        } else if (s.op == OPERATOR::DIV) {
            n = stk.Pop();
            stk.Push(stk.Pop() / n);
        } else if (s.op == OPERATOR::MUL) {
            stk.Push(stk.Pop() * stk.Pop());
        } else if (s.op == OPERATOR::UNARY_MINUS) {
            stk.Push(-stk.Pop());
        }
    }
    return stk.Pop();
}

//-----  Global Function Evaluate an Expression Tree
double Evaluate(Expr* r)
{
    return Evaluate(ExprList(r));
}

template<typename R, typename F>
R Map(R r, F&& fn)
{
    std::transform(std::begin(r), std::end(r), std::begin(r), std::forward<F>(fn));
    return r;
}

template<typename R, typename F>
R Filter(R r, F&& fn)
{
    R ret(r.size());
    auto first = std::begin(r), last = std::end(r), result = std::begin(ret);
    bool inserted = false;
    while (first != last) {
        if (fn(*first)) {
            *result = *first;
            inserted = true;
            ++result;
        }
        ++first;
    }
    if (!inserted) {
        ret.clear();
        ret.resize(0);
    }
    return ret;
}

int main(int, char**)
{
    unique_ptr<Expr> a(new BinaryExpr(new Number(10), new Number(20), OPERATOR::PLUS));
    unique_ptr<IExprVisitor> eval(new TreeEvaluatorVisitor());
    double result = a->accept(*eval);
    cout << "Output is => " << result << endl;
    unique_ptr<IExprVisitor> exp(new ReversePolishEvaluator());
    a->accept(*exp);

    std::cout << '\n';

    a.reset(new BinaryExpr(new Number(10), new Number(20), OPERATOR::PLUS));
    result = Evaluate(&(*a));
    cout << result << endl;
}

FlattenVisitor::~FlattenVisitor()
{}
