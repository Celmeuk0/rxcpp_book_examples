import qbs

Project {
//    SubProject {
//        filePath: "cpp_type_example/cpp_type_example.qbs"
//    }

    Product {
        name: "00_settings"
        Export {
            Depends { name: "cpp" }
            cpp.cxxLanguageVersion: "c++17"
            Group {     // Properties for the produced executable
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
            }
        }
    }

    CppApplication {
        files: [
            "observer.cpp",
        ]
        name: "01_observer"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "calculator.cpp",
        ]
        name: "02_calculator"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

}
