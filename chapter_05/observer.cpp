//-------------------- Observer.cpp
#include <iostream>
#include <memory>
#include <vector>
using namespace std;
//---- Forward declaration of event sink
template<class T>
class EventSourceValueObserver;
//----------A toy implementation of EventSource
template<class T>
class EventSourceValueSubject
{
    vector<EventSourceValueObserver<T>*> sinks;
    T State; // T is expected to be a value type
  public:
    EventSourceValueSubject() { State = 0; }
    ~EventSourceValueSubject() { sinks.clear(); }
    bool Subscribe(EventSourceValueObserver<T>* sink)
    { sinks.push_back(sink); return true; }
    void NotifyAll()
    {
        for (auto sink : sinks) {
            sink->Update(State);
        }
    }
    T GetState() { return State; }
    void SetState(T pstate)
    {
        State = pstate;
        NotifyAll();
    }
};

//--------------------- An event sink class for the preceding EventSources
template<class T>
class EventSourceValueObserver
{
    T OldState;

  public:
    EventSourceValueObserver() { OldState = 0; }
    virtual ~EventSourceValueObserver() {}
    virtual void Update(T State)
    {
        cout << "Old State " << OldState << endl;
        OldState = State;
        cout << "Current State " << State << endl;
    }
};

//------------ A simple specialized Observe
class AnotherObserver : public EventSourceValueObserver<double>
{
  public:
    AnotherObserver() : EventSourceValueObserver() {}
    virtual ~AnotherObserver();

    virtual void Update(double State)
    {
        cout << " Specialized Observer" << State << endl;
    }
};

AnotherObserver::~AnotherObserver() {}

int main()
{
    unique_ptr<EventSourceValueSubject<double>> evsrc(
      new EventSourceValueSubject<double>());

    //---- Create Two instance of Observer and Subscribe
    unique_ptr<AnotherObserver> evobs(new AnotherObserver());
    unique_ptr<EventSourceValueObserver<double>> evobs2(
      new EventSourceValueObserver<double>());

    evsrc->Subscribe(evobs.get());
    evsrc->Subscribe(evobs2.get());

    //------ Change the State of the EventSource
    //------ This should trigger call to Update of the Sink
    evsrc->SetState(100);
}
