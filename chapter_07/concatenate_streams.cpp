//------------- Concactatenate.cpp
//#include "rxcpp/rx-test.hpp"
#include "rxcpp/rx.hpp"

#define TRACE() std::cout << __PRETTY_FUNCTION__ << ':' << __LINE__ << '\n'

int main()
{
    // Code below fails to compile
    //    auto values_1 = rxcpp::observable<>::range(1);
    //    auto s1_1 = values_1.take(3).map([](int prime) { TRACE(); return 2 * prime; });
    //    auto s2_1 = values_1.take(3).map([](int prime) { TRACE(); return prime * prime;
    //    });
    //    s1_1.concat(s2_1).subscribe(rxcpp::util::apply_to([](int p) { printf("
    //    %d\n", p); }));

    //    auto o = s1.concat(s2);
    //    o.subscribe([](int v) { TRACE(); printf("OnNext: %d\n", v); },
    //                []() { TRACE(); printf("OnCompleted\n"); });

    //    s1.concat(s2).subscribe([](int v) { TRACE(); printf("OnNext: %d\n", v); },
    //                            []() { TRACE(); printf("OnCompleted\n"); });

    auto values =
      rxcpp::observable<>::range(1); // infinite (until overflow) stream of integers

    auto s1 = values.take(3).map([](int prime) { return std::make_tuple("1:", prime); });
    auto s2 = values.take(3).map([](int prime) { return std::make_tuple("2:", prime); });

    std::cout << "Concatenate:\n";
    s1.concat(s2).subscribe(
      rxcpp::util::apply_to([](const char* s, int p) { printf("%s %d\n", s, p); }));

    std::cout << "\nMerge:\n";
    s1.merge(s2).subscribe(
      rxcpp::util::apply_to([](const char* s, int p) { printf("%s %d\n", s, p); }));
}
