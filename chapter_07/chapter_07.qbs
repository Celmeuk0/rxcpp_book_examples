import qbs

Project {
//    SubProject {
//        filePath: "cpp_type_example/cpp_type_example.qbs"
//    }

    Product {
        name: "00_settings"
        Export {
            Depends { name: "cpp" }
            cpp.cxxLanguageVersion: "c++11"
            Group {     // Properties for the produced executable
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
            }
        }
    }

    CppApplication {
        files: [
            "rxcpp_first.cpp",
        ]
        name: "01_rxcpp_first"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "rxcpp_second.cpp",
        ]
        name: "02_rxcpp_second"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "stl_container_stream.cpp",
        ]
        name: "03_stl_container_stream"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "observer_from_scratch.cpp",
        ]
        name: "04_observer_from_scratch"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "concatenate_streams.cpp",
        ]
        name: "05_concatenate_streams"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "unsubscribe.cpp",
        ]
        name: "06_unsubscribe"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "average.cpp",
        ]
        name: "07_average"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "scan.cpp",
        ]
        name: "08_scan"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "map_with_pipe.cpp",
        ]
        name: "09_map_with_pipe"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "observe_on.cpp",
        ]
        name: "10_observe_on"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "subscribe_on.cpp",
        ]
        name: "11_subscribe_on"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "flatmap.cpp",
        ]
        name: "12_flatmap"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "concatmap.cpp",
        ]
        name: "13_concatmap"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "concat.cpp",
        ]
        name: "14_concat"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "merge.cpp",
        ]
        name: "15_merge"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "tap_example.cpp",
        ]
        name: "16_tap_example"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "defer.cpp",
        ]
        name: "17_defer"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "buffer.cpp",
        ]
        name: "18_buffer"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "timer.cpp",
        ]
        name: "19_timer"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }
}
