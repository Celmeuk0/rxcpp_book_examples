//------------------ Map_With_Pipe.cpp
#include "rxcpp/rx-test.hpp"
#include "rxcpp/rx.hpp"
#include <iostream>

namespace Rx {
using namespace rxcpp;
using namespace rxcpp::sources;
using namespace rxcpp::operators;
using namespace rxcpp::util;
}
using namespace Rx;

int main()
{
    //---------- chain map to the range using the pipe operator
    //----------- avoids the use of . notation.
    auto ints = rxcpp::observable<>::range(1, 10)
                | map([](int n) { return n * n; });

    ints.subscribe([](int v) { printf("OnNext: %d\n", v); },
                   []() { printf("OnCompleted\n"); });
}
