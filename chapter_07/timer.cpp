//----------- TimerExample.cpp
#include "rxcpp/rx.hpp"
#include <iostream>
int main()
{
    auto Scheduler = rxcpp::observe_on_new_thread();
    auto period = std::chrono::milliseconds(1);
    auto values = rxcpp::observable<>::timer(period, Scheduler).finally([]() {
        printf("The final action\n");
    });
    values.as_blocking().subscribe([](int v) { printf("OnNext: %d\n", v); },
                                   []() { printf("OnCompleted\n"); });
}
