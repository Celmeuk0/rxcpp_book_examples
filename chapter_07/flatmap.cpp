//----------- Flatmap.cpp
#include "rxcpp/rx-test.hpp"
#include "rxcpp/rx.hpp"
#include <array>
#include <iostream>

namespace rxu = rxcpp::util;

int main()
{
    std::array<std::string, 4> a = {{"Praseed", "Peter", "Sanjay", "Raju"}};
    //---------- Apply Flatmap on the array of names
    //---------- Flatmap returns an Observable<T> ( map returns T )
    //---------- The First lamda creates a new Observable<T>
    //---------- The Second Lambda manipulates primary Observable and
    //---------- Flatmapped Observable
    auto values = rxcpp::observable<>::iterate(a).flat_map(
      [](std::string v) {
          std::cout << "v = " << v << "\n";
          std::array<std::string, 3> salutation = {{"Mr.", "Monsieur", "Sri"}};
          return rxcpp::observable<>::iterate(salutation);
      },
      [](std::string f, std::string s) { return s + " " + f; });
    //-------- As usual subscribe
    //-------- Here the value will be interleaved as flat_map merges the
    //-------- Two Streams
    values.subscribe([](std::string f) { std::cout << f << std::endl; },
                     []() { std::cout << "Hello World.." << std::endl; });
}
