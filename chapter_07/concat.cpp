//---------------- Concat.cpp
#include "rxcpp/rx-test.hpp"
#include "rxcpp/rx.hpp"
#include <array>
#include <iostream>
int main()
{
    auto o1 = rxcpp::observable<>::range(1, 3);
    auto o2 = rxcpp::observable<>::from(4, 6);
    auto values = o1.concat(o2);
    values.subscribe([](int v) { printf("OnNext: %d\n", v); },
                     []() { printf("OnCompleted\n"); });
}
