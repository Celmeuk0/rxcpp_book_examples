// ---------------- FuncWrapper.cpp Requires C++ 17 (-std=c++1z )
#include <functional>
#include <iostream>
using namespace std;
// -------------- Simple Function call
void PrintNumber(int val)
{
    cout << val << endl;
}

// ------------------ A class which overloads function operator
struct PrintNumber {
    void operator()(int i) const
    {
        std::cout << i << '\n';
    }
};
// ------------ To demonstrate the usage of method call
struct FooClass {
    int number;
    FooClass(int pnum) : number(pnum)
    {
    }

    void PrintNumber(int val) const
    {
        std::cout << number + val << endl;
    }
};

int main()
{
    // ----------------- Ordinary Function Wrapped
    std::function<void(int)> displaynum = PrintNumber;
    displaynum(1);
    std::invoke(displaynum, 2); // call through std::invoke

    // -------------- Lambda Functions Wrapped
    std::function<void()> lambdaprint = []() { PrintNumber(3); };
    lambdaprint();
    std::invoke(lambdaprint);

    // Wrapping member functions of a class
    std::function<void(const FooClass&, int)> displayclass = &FooClass::PrintNumber;

    // creating an instance
    const FooClass fooinstance(4);
    displayclass(fooinstance, 5);
}
