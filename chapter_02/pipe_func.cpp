// ---- PipeFunc2.cpp
// -------- g++ -std=c++1z PipeFunc2.cpp
#include <iostream>
using namespace std;

#define TRACE()  std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl

struct AddOne {
    template<class T>
    auto operator()(T x) const
    {
        TRACE();
        std::cout << "x = " << x << std::endl;
        return x + 1;
    }
};

struct SumFunction {
    // Binary Operator
    template<class T>
    auto operator()(T x, T y) const
    {
        TRACE();
        std::cout << "x = " << x << ", y = " << y << std::endl;
        return x + y;
    }
};

// -------------- Create a Pipable Closure Function (Unary)
// -------------- Uses Variadic Templates Paramter pack
template<class F>
struct PipableClosure : F {
    template<class ... Xs>
    PipableClosure(Xs&& ... xs) :
        // Xs is a universal reference
        F(std::forward<Xs>(xs) ...)  // perfect forwarding
    {
        TRACE();
    }
};

// ---------- A helper function which converts a Function to a Closure
template<class F>
auto MakePipeClosure(F f)
{
    TRACE();
    return PipableClosure<F>(std::move(f));
}

// ------------ Declare a Closure for Binary
// ------------- Functions
//
template<class F>
struct PipableClosureBinary {
    template<class ... Ts>
    auto operator()(Ts ... xs) const
    {
        TRACE();
        return MakePipeClosure(
            [=](auto x) -> decltype(auto)
            {
                TRACE();
                return F()(x, xs ...);
            });
    }
};

// ------- Declare a pipe operator
// ------- uses perfect forwarding to invoke the function
template<class T, class F>   // ---- Declare a pipe operator
decltype(auto) operator| (T && x, const PipableClosure<F>& pfn)
{
    TRACE();
    return pfn(std::forward<T>(x));
}

int main()
{
    // -------- Declare a Unary Function Closure
    TRACE();
    const PipableClosure<AddOne> fnclosure = {};
    TRACE();
    int value = 1 | fnclosure | fnclosure;
    TRACE();
    std::cout << value << std::endl;

    // --------- Decalre a Binary function closure
    TRACE();
    const PipableClosureBinary<SumFunction> sumfunction = {};
    TRACE();
    int value1 = 1 | sumfunction(2) | sumfunction(5) | fnclosure;
    TRACE();
    std::cout << value1 << std::endl;
    TRACE();
}
