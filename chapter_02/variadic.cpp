// Variadic.cpp
#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

// --- add given below is a base case for ending compile time
// --- recursion
int add()
{
    return 0;
} // end condition

// ---- Declare a Variadic function Template
// ---- ... is called parameter pack. The compiler
// --- synthesize a function based on the number of arguments
// ------ given by the programmer.
// ----- decltype(auto) => Compiler will do Type Inference
template<class T0, class ... Ts>
decltype(auto) add(T0 first, Ts ... rest)
{
    return first + add(rest ...);
}

int main()
{
    int n = add(0, 2, 3, 4);
    std::cout << n << '\n';
}
