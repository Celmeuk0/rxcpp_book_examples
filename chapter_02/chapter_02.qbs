import qbs

Project {
//    SubProject {
//        filePath: "cpp_type_example/cpp_type_example.qbs"
//    }

    Product {
        name: "00_settings"
        Export {
            Depends { name: "cpp" }
            cpp.cxxLanguageVersion: "c++17"
            Group {     // Properties for the produced executable
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
            }
        }
    }

    CppApplication {
        name: "01_cpp_type_example"
        consoleApplication: true
        files: "cpp_type_example.cpp"
        Depends { name: "00_settings" }
    }

    CppApplication {
        name: "02_decltype"
        consoleApplication: true
        files: [ "decltype.cpp" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [ "first_auto.cpp", ]
        name: "03_first_auto"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [ "second_auto.cpp", ]
        name: "04_second_auto"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "uniform_initialization.cpp",
        ]
        name: "05_uniform_initialization"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "variadic.cpp",
        ]
        name: "06_variadic"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "variadic_2.cpp",
        ]
        name: "07_variadic_2"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "lvalue.cpp",
        ]
        name: "08_lvalue"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "rvalueref.cpp",
        ]
        name: "09_rvalueref"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "rvaluerefcall.cpp",
        ]
        name: "10_rvaluerefcall"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "floatbuffer.cpp",
        ]
        name: "11_floatbuffer"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "unique_ptr.cpp",
        ]
        name: "12_unique_ptr"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "shared_ptr.cpp",
        ]
        name: "13_shared_ptr"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "weak_ptr.cpp",
        ]
        name: "14_weak_ptr"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "lambda_1.cpp",
        ]
        name: "15_lambda_1"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "lambda_2.cpp",
        ]
        name: "16_lambda_2"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "lambda_3.cpp",
        ]
        name: "17_lambda_3"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "lambda_4.cpp",
        ]
        name: "18_lambda_4"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "compose.cpp",
        ]
        name: "19_compose"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "func_wrappers.cpp",
        ]
        name: "20_func_wrappers"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "pipe_func.cpp",
        ]
        name: "21_pipe_func"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "folds.cpp",
        ]
        name: "22_folds"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "variant.cpp",
        ]
        name: "23_variant"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "iter_observable.cpp",
        ]
        name: "24_iter_observable"
        consoleApplication: true
        Depends { name: "00_settings" }
    }
}
