// ------- Weak_Ptr.cpp
#include <iostream>
#include <deque>
#include <memory>

using namespace std;
int main()
{
    std::shared_ptr<int> ptr_1(new int(500));
    cout << __func__ << " s " << ptr_1.use_count() << '\n';
    std::weak_ptr<int> wptr_1 = ptr_1;
    {
        cout << __func__ << " w " << wptr_1.use_count() << '\n';
        cout << __func__ << " s " << ptr_1.use_count() << '\n';
        std::shared_ptr<int> ptr_2 = wptr_1.lock();
        cout << __func__ << " s " << ptr_1.use_count() << '\n';
        if (ptr_2) {
            cout << *ptr_2 << endl; // this will be exeucted
        }
        // ---- ptr_2 will go out of the scope
    }

    ptr_1.reset(); // Memory is deleted.
    cout << __func__ << " s " << ptr_1.use_count() << '\n';
    cout << __func__ << " w " << wptr_1.use_count() << '\n';

    std::shared_ptr<int> ptr_3 = wptr_1.lock();
    cout << __func__ << " w " << wptr_1.use_count() << '\n';
    cout << __func__ << " s " << ptr_1.use_count() << '\n';
    cout << __func__ << " s " << ptr_3.use_count() << '\n';
    // -------- Always else part will be executed
    // -------- as ptr_3 is nullptr now
    if (ptr_3) {
        cout << *ptr_3 << endl;
    } else {
        cout << "Defunct Pointer" << endl;
    }
    return 0;
}
