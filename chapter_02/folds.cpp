//---------------- Folds.cpp
//--------------- Requires C++ 17 (-std=c++1z )
//--------------- http://en.cppreference.com/w/cpp/language/fold
#include <functional>
#include <iostream>

using namespace std;
template<typename... Ts>
auto AddFoldLeftUn(Ts... args)
{
    return (... + args);
}
template<typename... Ts>
auto AddFoldLeftBin(int n, Ts... args)
{
    return (n + ... + args);
}
template<typename... Ts>
auto AddFoldRightUn(Ts... args)
{
    return (args + ...);
}
template<typename... Ts>
auto AddFoldRightBin(int n, Ts... args)
{
    return (args + ... + n);
}
template<typename T, typename... Ts>
auto AddFoldRightBinPoly(T n, Ts... args)
{
    return (args + ... + n);
}
template<typename T, typename... Ts>
auto AddFoldLeftBinPoly(T n, Ts... args)
{
    return (n + ... + args);
}

template<typename... Args>
void printer(Args&&... args)
{
    (std::cout << ... << args) << '\n';
}

int main()
{
    auto a = AddFoldLeftUn(1, 2, 3, 4);
    cout << a << endl;
    cout << AddFoldRightBin(a, 4, 5, 6) << endl;
    //---------- Folds from Right
    //---------- should produce "Hello  World C++"
    auto b = AddFoldRightBinPoly("C++ "s, "Hello "s, "World "s);
    cout << b << endl;
    //---------- Folds (Reduce) from Left
    //---------- should produce "Hello World C++"
    auto c = AddFoldLeftBinPoly("Hello "s, "World "s, "C++ "s);
    cout << c << endl;
    printer(1, 2, 3, 4);
}
