///---- Rvaluref.cpp
#include <iostream>

using namespace std;

int main()
{
    int&& j = 42;
    int x = 3;
    int y = 5;
    int&& z = x + y;

    cout << z << endl;
    z = 10;
    cout << z << endl;
    cout << j << endl;
    j = x;
    j = 17;
    cout << x << endl;
    cout << j << endl;
}
