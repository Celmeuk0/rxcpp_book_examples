// ------- RvaluerefCall.cpp
#include <iostream>

using namespace std;

void TestFunction(int& a)
{
    cout << "references" << endl;
    cout << a << endl;
}

void TestFunction(int&& a)
{
    cout << "rvalue references" << endl;
    cout << a << endl;
}

int main()
{
    int&& j = 42;
    int x = 3, y = 5;
    int&& z = x + y;
    TestFunction(x + y);  // Should call rvalue reference function
    TestFunction(std::move(z));
    TestFunction(std::move(x));
    TestFunction(j); // Calls Lvalue Refreence function
}
