#include <atomic>
#include <iostream>

int main()
{
    std::atomic<int> value;

    std::cout << "Result returned from Operation fetch_add(5): " << value.fetch_add(5) << '\n';
    std::cout << "Result after Operation fetch_add(5): " << value << '\n';

    std::cout << "Result returned from Operation fetch_sub(3): " << value.fetch_sub(3) << '\n';
    std::cout << "Result after Operation fetch_sub(3): " << value << '\n';

    std::cout << "Result returned from Operation value++: " << value++ << '\n';
    std::cout << "Result after Operation value++: " << value << '\n';

    std::cout << "Result returned from Operation ++value: " << ++value << '\n';
    std::cout << "Result after Operation ++value: " << value << '\n';

    value += 1;
    std::cout << "Result after Operation += 1: " << value << '\n';

    value -= 1;
    std::cout << "Result after Operation -= 1: " << value << '\n';

    return 0;
}
