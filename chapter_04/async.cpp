#include <future>
#include <iostream>
#include <numeric>
#include <thread>
#include <vector>

// Function to calculate the sum of elements in a vector
int calc_sum(std::vector<int> v)
{
    std::cout << std::this_thread::get_id() << '\n';
    int sum = std::accumulate(v.begin(), v.end(), 0);
    return sum;
}

int main()
{
    std::vector<int> nums{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::cout << std::this_thread::get_id() << '\n';
    // task launch using std::async
    std::future<int> result(std::async(std::launch::async, calc_sum, std::move(nums)));

    // Fetch the result of async, the value returned by calc_sum()
    int sum = result.get();

    std::cout << "Sum = " << sum << std::endl;

    // Fetch associated future from async
    result =
      (async([](std::vector<int> v) { return std::accumulate(v.begin(), v.end(), 0); },
             std::move(nums)));

    return 0;
}
