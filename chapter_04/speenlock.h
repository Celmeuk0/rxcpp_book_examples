#pragma once

#include <atomic>

class spin_lock
{
    std::atomic_flag flg = ATOMIC_FLAG_INIT;

  public:
    spin_lock() {}

    void lock()
    {
        // acquire lock and spin
        while (flg.test_and_set(std::memory_order_acquire))
            ;
    }

    void unlock()
    {
        // release lock
        flg.clear(std::memory_order_release);
    }
};
