import qbs

Project {
//    SubProject {
//        filePath: "cpp_type_example/cpp_type_example.qbs"
//    }

    Product {
        name: "00_settings"
        Export {
            Depends { name: "cpp" }
            cpp.cxxLanguageVersion: "c++17"
            Group {     // Properties for the produced executable
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
            }
        }
    }

    CppApplication {
        files: [
            "packaged_task.cpp",
        ]
        name: "01_packaged_task"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "async.cpp",
        ]
        name: "02_async"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "atomic_int.cpp",
        ]
        name: "03_atomic_int"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "seq_cst.cpp",
        ]
        name: "04_seq_cst"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "speenlock.h",
            "speenlock_main.cpp",
        ]
        name: "05_acquire_release"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "relaxed.cpp",
        ]
        name: "06_relaxed"
        consoleApplication: true
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "lock_free_queue.h",
            "lock_free_queue_main.cpp",
        ]
        name: "07_lock_free_queue"
        consoleApplication: true
        Depends { name: "00_settings" }
    }
}
