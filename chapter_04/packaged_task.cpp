#include <future>
#include <iostream>
#include <numeric>
#include <vector>

#define TRACE() std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << std::endl

void print_v(const std::vector<int>& v)
{
    for (const auto& i : v) {
        std::cout << i << ' ';
    }
    std::cout << '\n';
}

// Function to calculate the sum of elements in an integer vector
int calc_sum(std::vector<int> v)
{
    TRACE();
    int sum = std::accumulate(v.begin(), v.end(), 0);
    return sum;
}

int main()
{
    // Creating a packaged_task encapsulates a function
    std::packaged_task<int(std::vector<int>)> task(calc_sum);

    // Fetch associated future from packaged_task
    std::future<int> result = task.get_future();

    std::vector<int> nums{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    // Pass packaged_task to thread to run asynchronously
    std::thread t(std::move(task), std::move(nums));

    t.join();
    // Fetch the result of packaged_task, the value returned by calc_sum()
    int sum = result.get();

    std::cout << "Sum = " << sum << std::endl;

    std::packaged_task<int(std::vector<int>)> lambda_task([](std::vector<int> v) {
        TRACE();
        print_v(v);
        return std::accumulate(v.begin(), v.end(), 0);
    });

    nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::future<int> l_result = lambda_task.get_future();
    t = std::thread(std::move(lambda_task), std::move(nums));

    t.join();
    // Fetch the result of packaged_task, the value returned by calc_sum()
    sum = l_result.get();

    std::cout << "Sum = " << sum << std::endl;

    return 0;
}
