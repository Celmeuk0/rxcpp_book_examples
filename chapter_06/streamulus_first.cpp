#include <iostream>
#include <streamulus.h>

using namespace std;
using namespace streamulus;

struct print
{
    static double temp;
    print() {}
    template<typename T>
    T operator()(const T& value) const
    {
        print::temp += value;
        std::cout << print::temp << std::endl;
        return value;
    }
};

double print::temp = 0;

void hello_Stream()
{
    using namespace streamulus;
    // Define an input Stream of strings, whose name is "Input Stream"
    InputStream<double> s = NewInputStream<double>("Input Stream", true /* verbose */);
    // Construct a Streamulus instance
    Streamulus Streamulus_engine;
    // For each element of the Stream:
    //     aggregate the received value into a running sum
    //     print it
    Streamulus_engine.Subscribe(Streamify<print>(s));
    // Insert data to the input Stream
    InputStreamPut<double>(s, 10);
    InputStreamPut<double>(s, 20);
    InputStreamPut<double>(s, 30);
}

int main()
{
    hello_Stream();
    return 0;
}
