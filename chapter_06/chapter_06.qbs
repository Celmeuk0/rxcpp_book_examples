import qbs

Project {
//    SubProject {
//        filePath: "cpp_type_example/cpp_type_example.qbs"
//    }

    Product {
        name: "00_settings"
        Export {
            Depends { name: "cpp" }
            cpp.cxxLanguageVersion: "c++14"
            Group {     // Properties for the produced executable
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
            }
        }
    }

    CppApplication {
        files: [
            "streams_first.cpp",
        ]
        name: "01_streams_first"
        consoleApplication: true
        cpp.includePaths: [ "../Streams/source" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "streams_second.cpp",
        ]
        name: "02_streams_second"
        consoleApplication: true
        cpp.includePaths: [ "../Streams/source" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "streams_third.cpp",
        ]
        name: "03_streams_third"
        consoleApplication: true
        cpp.includePaths: [ "../Streams/source" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "streamulus_first.cpp",
        ]
        name: "04_streamulus_first"
        consoleApplication: true
        cpp.includePaths: [ "../streamulus/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "streamulus_second.cpp",
        ]
        name: "05_streamulus_second"
        consoleApplication: true
        cpp.includePaths: [ "../streamulus/src" ]
        Depends { name: "00_settings" }
    }
}
