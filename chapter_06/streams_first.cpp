//--------- Streams_First.cpp
#include <Stream.h>

using namespace std;
using namespace stream;
using namespace stream::op;

int main()
{
    //-------- counter(n) - Generate a series of value
    //-------- Map (Apply a Lambda)
    //-------- limit(n) -- Take first ten items
    //-------- Sum -- aggregate
    int total = MakeStream::counter(1)
                | map_([](int x) { std::cout << x << " "; return x * x; }) // Apply square on each elements
                | limit(10)                         // take first ten elements
                | sum(); // sum the Stream contents Streams::op::sum
    //----------- print the result
    cout << total << endl;
}
