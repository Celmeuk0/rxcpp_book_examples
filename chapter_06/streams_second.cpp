//--------------- Streams_Second.cpp
// g++ -I./Streams-master/sources Streams_Second.cpp
//
#include <Stream.h>
#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

using namespace std;
using namespace stream;
using namespace stream::op;

int main()
{
    std::vector<double> a = {10, 20, 30, 40, 50};
    //------------ Make a Stream and reduce
    auto val = MakeStream::from(a) | reduce(std::plus<void>());
    //------ Compute the arithematic average
    cout << val / a.size() << endl;
}
