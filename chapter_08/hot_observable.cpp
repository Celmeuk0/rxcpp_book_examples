//---------- HotObservable.cpp

#include <memory>
#include <rxcpp/rx.hpp>
int main(int /*argc*/, char* /*argv*/[])
{
    auto eventloop = rxcpp::observe_on_event_loop();
    //----- Create a Cold Observable
    //----- Convert Cold Observable to Hot Observable
    //----- using .Publish();
    auto values =
      rxcpp::observable<>::interval(std::chrono::seconds(2)).take(5).publish();
    //----- Subscribe Twice
    values.subscribe_on(eventloop).subscribe([](int v) { printf("[1] onNext: %d\n", v); },
                                             []() { printf("[1] onCompleted\n"); });
    values.subscribe_on(eventloop).subscribe([](int v) { printf("[2] onNext: %d\n", v); },
                                             []() { printf("[2] onCompleted\n"); });
    values.subscribe_on(eventloop).subscribe([](int v) { printf("[4] onNext: %d\n", v); },
                                             []() { printf("[4] onCompleted\n"); });
    //------ Connect to Start Emitting Values
    values.connect();
    // Don't wait before subscribing
    values.as_blocking().subscribe([](long v) { printf("[3] OnNext: %ld\n", v); },
                                   []() { printf("[3] OnCompleted\n"); });
//    //---- make a blocking subscription to see the results
//    values.as_blocking().subscribe();
    //----------- Wait for Two Seconds
    rxcpp::observable<>::timer(std::chrono::milliseconds(2000)).subscribe([&](long) {});
}
