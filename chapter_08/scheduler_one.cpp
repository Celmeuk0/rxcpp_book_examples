//------------- SchedulerOne.cpp
#include "rxcpp/rx.hpp"
int main()
{
    //---------- Get a Coordination
    auto coordination = rxcpp::serialize_new_thread();
    //------- Create a Worker instance  through a factory method
    auto worker = coordination.create_coordinator().get_worker();
    //--------- Create a action object
    auto sub_action =
      rxcpp::schedulers::make_action([](const rxcpp::schedulers::schedulable&) {
          std::cout << "Action Executed in Thread # : " << std::this_thread::get_id()
                    << '\n';
      });
    //------------- Create a schedulable and schedule the action
    auto scheduled = rxcpp::schedulers::make_schedulable(worker, sub_action);
    scheduled.schedule();
    return 0;
}
