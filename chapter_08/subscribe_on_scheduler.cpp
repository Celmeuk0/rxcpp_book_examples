//-------- SubscribeOnScheduler.cpp
#include "rxcpp/rx.hpp"
int main()
{
    //------- Print the main thread id
    std::cout << "Main Thread Id is " << std::this_thread::get_id() << '\n';
    //-------- We are using subscribe_on here
    //-------- The Map and subscribed Lambda will
    //--------- use the secondary thread
    rxcpp::observable<>::range(0, 15)
      .map([](int i) {
          std::cout << "Map " << std::this_thread::get_id() << " : " << i << '\n';
          return i;
      })
      .take(5)
      .subscribe_on(rxcpp::synchronize_new_thread())
      .subscribe([&](int i) {
          std::cout << "Subs " << std::this_thread::get_id() << " : " << i << '\n';
      });
    //----------- Wait for Two Seconds
    rxcpp::observable<>::timer(std::chrono::milliseconds(2000)).subscribe([&](long) {});

    return 0;
}
