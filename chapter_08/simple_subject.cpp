//------- SimpleSubject.cpp
#include <memory>
#include <rxcpp/rx.hpp>
int main(int /*argc*/, char* /*argv*/[])
{
    //----- Create an instance of Subject
    rxcpp::subjects::subject<int> subject;
    //----- Retreive the Observable
    //----- attached to the Subject
    auto observable = subject.get_observable();
    //------ Subscribe Twice
    observable.subscribe([](int v) { printf("1------%dn", v); });
    observable.subscribe([](int v) { printf("2------%dn", v); });
    //--------- Get the Subscriber Interface
    //--------- Attached to the Subject
    auto subscriber = subject.get_subscriber();
    //----------------- Emit Series of Values
    subscriber.on_next(1);
    subscriber.on_next(4);
    subscriber.on_next(9);
    subscriber.on_next(16);
    //----------- Wait for Two Seconds
    rxcpp::observable<>::timer(std::chrono::milliseconds(2000)).subscribe([&](long) {});
}
