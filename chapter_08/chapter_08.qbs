import qbs

Project {
//    SubProject {
//        filePath: "cpp_type_example/cpp_type_example.qbs"
//    }

    Product {
        name: "00_settings"
        Export {
            Depends { name: "cpp" }
            cpp.cxxLanguageVersion: "c++11"
            cpp.dynamicLibraries: [ "pthread" ]
            Group {     // Properties for the produced executable
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
            }
        }
    }

    CppApplication {
        files: [
            "cold_observable.cpp",
        ]
        name: "01_cold_observable"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "hot_observable.cpp",
        ]
        name: "02_hot_observable"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "hot_observable_2.cpp",
        ]
        name: "02_hot_observable_2"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "replay_all.cpp",
        ]
        name: "03_replay_all"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "subscriber.cpp",
        ]
        name: "04_subscriber"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "simple_subject.cpp",
        ]
        name: "05_simple_subject"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "behaviour_subject.cpp",
        ]
        name: "06_behaviour_subject"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "replay_subject.cpp",
        ]
        name: "07_replay_subject"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "scheduler_one.cpp",
        ]
        name: "08_scheduler_one"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "scheduler_two.cpp",
        ]
        name: "09_scheduler_two"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "observable_on_scheduler.cpp",
        ]
        name: "10_observable_on_scheduler"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "subscribe_on_scheduler.cpp",
        ]
        name: "11_subscribe_on_scheduler"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }

    CppApplication {
        files: [
            "run_loop.cpp",
        ]
        name: "12_run_loop"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        cpp.dynamicLibraries: [ "pthread" ]
        Depends { name: "00_settings" }
    }
}
