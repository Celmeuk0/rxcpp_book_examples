import qbs

Project {
    Product {
        name: "00_settings"
        Export {
            Depends { name: "cpp" }
            cpp.cxxLanguageVersion: "c++11"
            Group {     // Properties for the produced executable
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
            }
        }
    }

    CppApplication {
        files: [
            "rxcpp_example.cpp",
        ]
        name: "01_rxcpp_example"
        consoleApplication: true
        cpp.includePaths: [ "../RxCpp/Rx/v2/src" ]
        Depends { name: "00_settings" }
    }
}
