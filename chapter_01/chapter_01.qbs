import qbs

Project {
    CppApplication {
        name: "rx_interfaces"
        consoleApplication: true
        files: [
            "main.cpp",
            "rx_interfaces.h",
        ]

        Group {     // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: true
            qbs.installDir: "bin"
        }
    }
}
