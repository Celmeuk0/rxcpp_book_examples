#pragma once

struct CustomException /*:*public std::exception */
{
    public:
        const char *what() const throw ()
        {
            return "C++ Exception";
        }
};

template<class T>
class IEnumerator
{
    public:
        virtual ~IEnumerator() = default;
        virtual bool HasMore() = 0;
        virtual T next() = 0;
};

template<class T>
class IEnumerable
{
    public:
        virtual ~IEnumerable() = default;
        virtual IEnumerator<T> *GetEnumerator() = 0;
};

template<class T>
class IObserver
{
    public:
        virtual ~IObserver() = default;
        virtual void OnCompleted() = 0;
        virtual void OnError(CustomException* exception) = 0;
        virtual void OnNext(T value) = 0;
};

template<typename T>
class IObservable
{
    public:
        virtual ~IObservable() = default;
        virtual bool Subscribe(IObserver<T>& observer) = 0;
};
