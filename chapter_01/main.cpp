#include "rx_interfaces.h"
#include <iostream>

class ConcreteEnumberable : public IEnumerable<int>
{
        int* numberlist, _count;
    public:
        ConcreteEnumberable(int numbers[], int count)
            : numberlist(numbers), _count(count) {}

        ~ConcreteEnumberable() = default;

        class Enumerator : public IEnumerator<int>
        {
                int* inumbers, icount, index;
            public:
                Enumerator(int* numbers, int count)
                    : inumbers(numbers), icount(count), index(0) {}

                ~Enumerator() = default;

                bool HasMore() { return index < icount; }

                // ---------- ideally speaking, the next function should throw
                // ---------- an exception...instead it just returns -1 when the
                // ---------- bound has reached
                int next() { return (index < icount) ? inumbers[index++] : -1; }
        };

        IEnumerator<int> *GetEnumerator()
        {
            return new Enumerator(numberlist, _count);
        }
};

class EvenNumberObservable : IObservable<int>
{
        int* _numbers, _count;
    public:
        EvenNumberObservable(int numbers[], int count) : _numbers(numbers),
            _count(count)
        {
        }

        bool Subscribe(IObserver<int>& observer)
        {
            for (int i = 0; i < _count; ++i) {
                if (_numbers[i] % 2 == 0) {
                    observer.OnNext(_numbers[i]);
                }
            }
            observer.OnCompleted();
            return true;
        }
};

class SimpleObserver : public IObserver<int>
{
    public:
        void OnNext(int value) { std::cout << value << std::endl; }
        void OnCompleted() { std::cout << "hello completed" << std::endl; }
        void OnError(CustomException*) {}
};

int main()
{
    int x[] = { 1,2,3,4,5 };
    //-------- Has used Raw pointers on purpose here as we have
    //------- not introduced unique_ptr,shared_ptr,weak_ptr yet
    //-------- using auto_ptr will be confusting...otherwise
    //-------- need to use boost library here... ( an overkill)
    ConcreteEnumberable *t = new ConcreteEnumberable(x, 5);
    IEnumerator<int> * numbers = t->GetEnumerator();
    while (numbers->HasMore())
          std::cout << numbers->next() << std::endl;
    delete numbers;
    delete t;

    EvenNumberObservable *eno = new EvenNumberObservable(x, 5);
    IObserver<int> *xy = new SimpleObserver();
    eno->Subscribe(*xy);
    delete xy;
    delete t;

    return 0;
}
